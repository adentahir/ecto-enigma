defmodule EctoEnigma.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do

    execute "CREATE EXTENSION IF NOT EXISTS citext", ""

    create table(:users) do
      add :name, :string
      add :email, :citext, null: false
      add :bio, :string
      add :number_of_pets, :integer
      add :hashed_password, :string, null: false
      add :confirmed_at, :naive_datetime

      timestamps()
    end
  end
end
