defmodule HelloMysqlWeb.PageController do
  use HelloMysqlWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
