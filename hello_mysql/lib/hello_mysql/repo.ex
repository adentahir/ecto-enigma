defmodule HelloMysql.Repo do
  use Ecto.Repo,
    otp_app: :hello_mysql,
    adapter: Ecto.Adapters.MyXQL
end
