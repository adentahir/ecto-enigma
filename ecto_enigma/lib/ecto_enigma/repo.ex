defmodule EctoEnigma.Repo do
  use Ecto.Repo,
    otp_app: :ecto_enigma,
    adapter: Ecto.Adapters.Postgres
end
