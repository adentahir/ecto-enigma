defmodule EctoEnigmaWeb.PageController do
  use EctoEnigmaWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
